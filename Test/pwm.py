import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
GPIO.setup(15, GPIO.OUT)

p = GPIO.PWM(15, 100)  # channel=12 frequency=50Hz
p.start(100)

try:
    while 1:
        pass
except KeyboardInterrupt:
    pass
p.stop()
GPIO.cleanup()