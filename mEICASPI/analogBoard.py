### ISL9023 Python Module
import smbus, array
import sys
import time

#Register Pointer Bytes


i2cbus = smbus.SMBus(1)

# define class
class init:
    def __init__(self,address):
         global dev_address
         dev_address = address

    def getDeviceName(self):   
        #10 byte
        i2cdataString = ["0","0","0","0","0","0","0","0","0","0"]
        i2cdata = [0,0,0,0,0,0,0,0,0,0]
        for i in range(0,10):
            i2cdata[i] = i2cbus.read_byte_data(dev_address, 0x01)
            i2cdataString[i] = chr(i2cdata[i])
        returnString = ""
        returnString = ''.join(i2cdataString)
        return returnString

    def getID(self):
        ID = i2cbus.read_byte_data(dev_address, 0x02)
        return ID

    def getHardwareID(self):
        hardVersion = i2cbus.read_byte_data(dev_address, 0x03)
        return hardVersion

    def getSoftwareID(self):
        softVersion = i2cbus.read_byte_data(dev_address, 0x04)
        return softVersion

    def getInputVoltage(self):
        voltage = i2cbus.read_word_data(dev_address, 0x05)
        return voltage

    def readNTC(self, number):
        NTCVal = i2cbus.read_word_data(dev_address, (number+0x05))
        return NTCVal

    def PressNTC(self, number):
        PressVal = i2cbus.read_word_data(dev_address, (number + 0xC))
        return PressVal

    def readTacho1(self):
        try:
            # Get the sensor value from the Arduino (signed 16bit little-endian)...
            Tacho1Val = i2cbus.read_word_data(dev_address, 0xE)
            return Tacho1Val
        except IOError:
            print ("*** error: receiving sensor value ***")
        

    def readTacho2(self):
        Tacho2Val = i2cbus.read_word_data(dev_address, 0xF)
        return Tacho2Val

    def getChargeStat(self):
        ChargeStat = i2cbus.read_byte_data(dev_address, 0x10)
        return ChargeStat

    def getBatteryStat(self):
        BatteryStat = i2cbus.read_byte_data(dev_address, 0x11)
        return BatteryStat

    def getMstCtn(self):
        MstCtnVal = i2cbus.read_byte_data(dev_address, 0x12)
        return MstCtnVal
        
    def getRotaryCount(self):
        RotaryCount = i2cbus.read_word_data(dev_address, 0x13)
        return RotaryCount
        
    def getEncoderButton(self):
        EncoderButton = i2cbus.read_byte_data(dev_address, 0x14)
        return EncoderButton    
